package net.p2pcdn.user.repository;

import net.p2pcdn.user.domain.User;

import java.util.List;
import java.util.Map;

public interface UserRepository {

    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(User record);

    User findByUsername(String username);

    List<User> query(Map<String, Object> params);
}