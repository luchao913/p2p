package net.p2pcdn.user.repository;

import net.p2pcdn.user.domain.Role;

import java.util.List;

public interface RoleRepository {


    int deleteByPrimaryKey(Integer id);

    int insert(Role record);

    Role selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(Role record);

    Role findByCode(String roleSuperAdmin);

    List<Role> findAll();
}