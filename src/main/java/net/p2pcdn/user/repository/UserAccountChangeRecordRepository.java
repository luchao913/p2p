package net.p2pcdn.user.repository;

import net.p2pcdn.user.domain.UserAccountChangeRecord;

import java.util.List;
import java.util.Map;

public interface UserAccountChangeRecordRepository {
    int insert(UserAccountChangeRecord changeRecord);

    List<UserAccountChangeRecord> query(Map<String, Object> params);
}
