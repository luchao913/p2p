package net.p2pcdn.user.repository;

import net.p2pcdn.user.domain.UserRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserRoleRepository {

    int deleteByPrimaryKey(Integer id);

    int insert(UserRole record);

    UserRole selectByPrimaryKey(Integer id);

    List<UserRole> findByUserId(int userId);

    int updateByPrimaryKey(UserRole record);

    int doesExist(@Param("userId") int userId,@Param("roleId") int roleId);

    void deleteByUserId(int userId);

    void deleteByUserIdAndRoleId(@Param("userId") int userId, @Param("roleId")int roleId);
}