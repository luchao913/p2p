package net.p2pcdn.user.repository;

import net.p2pcdn.user.domain.UserAccount;

public interface UserAccountRepository {
    UserAccount findByUserId(int userId);

    int insert(UserAccount userAccount);

    UserAccount selectByPrimaryKey(int id);

    int updateByPrimaryKey(UserAccount userAccount);
}
