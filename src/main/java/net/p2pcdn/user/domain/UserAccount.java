package net.p2pcdn.user.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户账号
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class UserAccount {
    private int id;
    private int userId;
    private BigDecimal balance;
    private Date createTime;
    private int version;
    private Date updateTime;

    public UserAccount(int userId, BigDecimal balance) {
        this.userId = userId;
        this.balance = balance;
        this.createTime = new Date();
        this.updateTime = new Date();
        this.version = 1;
    }
}
