package net.p2pcdn.user.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class User implements Serializable {
    private int id;
    private String username;
    private String email;
    private boolean emailValid;
    private String password;
    private boolean locked;
    private Date createTime;
    private Date updateTime;
    private String roleNames;//vo properties;
    private BigDecimal balance;

    public BigDecimal getBalance(){
        return balance == null ? new BigDecimal("0") :balance;
    }

    public User(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.createTime = new Date();
        this.updateTime = this.createTime;
        this.locked = false;
    }
}
