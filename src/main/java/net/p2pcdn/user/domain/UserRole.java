package net.p2pcdn.user.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zx
 */
@Getter
@Setter
public class UserRole implements Serializable {
    private static final long serialVersionUID = 7948392040798740913L;
    private int id;
    private int roleId;
    private int userId;
    private String roleName; //vo properties
    private String roleCode; //vo properties
    private String username;//vo properties

    public UserRole(int userId, int roleId) {
        super();
        this.roleId = roleId;
        this.userId = userId;
    }

    public UserRole() {
        super();
    }

}
