package net.p2pcdn.user.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author
 */
@Getter
@Setter
public class Role implements Serializable {
    private static final long serialVersionUID = -6280662222051796166L;

    public static final String ROLE_SUPER_ADMIN = "ROLE_SUPER_ADMIN";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_CUSTOMER_ADMIN = "ROLE_CUSTOMER_ADMIN";
    public static final String ROLE_USER = "ROLE_USER";
    private int id;
    private String code;
    private String name;

    public Role() {
        super();
    }

    public Role(String code, String name) {
        super();
        this.code = code;
        this.name = name;
    }

}
