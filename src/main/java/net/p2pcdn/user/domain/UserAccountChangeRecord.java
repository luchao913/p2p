package net.p2pcdn.user.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 账户变动明细
 */
@Getter
@Setter
@NoArgsConstructor
public class UserAccountChangeRecord {
    private int id;
    private int userAccountId;
    private BigDecimal delta;
    private boolean negative;
    private Date changeTime;
    private int bizType;
    private String remark;
    private BigDecimal balance;
    private Integer orderId;

    public static enum BizType {
        /**
         * 线下支付
         */
        OFF_LINE_PAY(10),
        /**
         *
         */
        ON_LINE_PURCHASE(11),
        /*
        *管理员赠送
         */
        FREE(12),
        EXPENSE(20);
        private final int value;
        BizType(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public UserAccountChangeRecord(int userAccountId, BigDecimal delta, boolean negative, int bizType,BigDecimal balance) {
        this.userAccountId = userAccountId;
        this.delta = delta;
        this.negative = negative;
        this.bizType = bizType;
        this.balance = balance;
        this.changeTime = new Date();
    }

    public BigDecimal getIncome(){
        return !this.negative ? delta : new BigDecimal("0");
    }

    public BigDecimal getExpense(){
        return this.negative ? delta : new BigDecimal("0");
    }
}
