package net.p2pcdn.email.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class EmailRecord {
    private String id;
    private Integer mailType;
    private String acceptEmail;//接收者
    private Integer senderId;//发送人
    private String subject; //主题
    private Date sendTime = new Date();
    private String content;
    private String checkCode;
}