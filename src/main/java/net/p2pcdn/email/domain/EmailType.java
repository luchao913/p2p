package net.p2pcdn.email.domain;

import lombok.Getter;

@Getter
public enum EmailType {
    REG(1, "邮箱注册"),
    VALIDATE(2, "邮箱验证"),
    VALIDATE_COMPANY(3, "激活公司"),
    RESET_PASSWORD(4, "找回密码"),
    UPDATE_PASSWORD(5, "修改密码");

    private final Integer type;
    private final String typeValue;

    EmailType(Integer type, String typeValue) {
        this.type = type;
        this.typeValue = typeValue;
    }

    public static String getNameByType(Integer type) {
        if (type == null)
            return "";
        for (EmailType t : values()) {
            if (t.type.intValue() == type.intValue())
                return t.typeValue;
        }
        return "";
    }
}
