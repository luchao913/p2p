package net.p2pcdn.email.repository;

import net.p2pcdn.email.domain.EmailRecord;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public interface EmailRecordDao {

    int insert(EmailRecord record);

    int queryByCheckCode(Map<String, Object> params);
}