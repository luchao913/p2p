package net.p2pcdn.email;

public class EmailSenderFailedException extends RuntimeException {
	
	private static final long serialVersionUID = -6081286118466058671L;
	
	public EmailSenderFailedException(){}
	
	public EmailSenderFailedException(String msg){
		super(msg);
	}
}
