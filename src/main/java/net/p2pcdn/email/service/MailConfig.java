package net.p2pcdn.email.service;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MailConfig implements Serializable{
	private static final long serialVersionUID = -1958148438455303061L;
	private  String user;
	private  String password;
	private  String sendName;
	private  boolean auth ;
	private  String smtpHost;
	private  String port;

}
