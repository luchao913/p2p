package net.p2pcdn.common;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AccessTokenResult implements Serializable {
    private String scope;
    private String access_token;
    private String app_id;
    private String token_type;
    private String nonce;
    private int expires_in;
}
