package net.p2pcdn.common;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author zx
 */
public class TrafficUnitUtils {
    private final static String BASE = "1024";

    public static BigDecimal toGB(BigDecimal input) {
        BigDecimal base = new BigDecimal(BASE);
        return input.divide(base, 8, RoundingMode.HALF_UP);
    }
}
