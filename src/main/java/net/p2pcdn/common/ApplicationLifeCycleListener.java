package net.p2pcdn.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ApplicationLifeCycleListener implements ServletContextListener {
    private Logger _logger = LoggerFactory.getLogger(this.getClass());
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        _logger.info("================Starting application.================");
        Application.setWebPhysicalPath(sce.getServletContext().getRealPath("/"));
        Application.setWebContextPath(sce.getServletContext().getContextPath());
       _logger.info("=============== WebPhysicalPath "+Application.getWebPhysicalPath()+" ==================");
       _logger.info(" ============== WebContextPath "+Application.getWebContextPath()+" ==================");
    }
    
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        _logger.info("================Application stopped.================");
    }
}
