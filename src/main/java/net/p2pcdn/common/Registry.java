package net.p2pcdn.common;

import com.qc.eventbus.EventBus;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;


public class Registry implements ApplicationContextAware {

    private static ApplicationContext applicationContext;
    
    public static <T> T getComponent(Class<T> requiredType) {
        return applicationContext.getBean(requiredType);
    }

    public static <T> T getComponent(String name,Class<T> requiredType) {
        return applicationContext.getBean(name,requiredType);
    }
    
    public static EventBus getEventBus() {
        return applicationContext.getBean(EventBus.class);
    }
    
    public synchronized void setApplicationContext(
            ApplicationContext applicationContext)
            throws BeansException {
        Registry.applicationContext = applicationContext;
    }
}
