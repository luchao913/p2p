/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.p2pcdn.common;

import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author zx
 */
public class Application {
    private static String webPhysicalPath;
    private static String webContextPath;
    
    public static String getWebPhysicalPath() {
        return webPhysicalPath;
    }
    
    protected static void setWebPhysicalPath(String path) {
        webPhysicalPath = path;
    }

    public static String getWebContextPath() {
        return webContextPath;
    }

    protected static void setWebContextPath(String webContextPath) {
        Application.webContextPath = webContextPath;
    }

    public static void main(String[] args) {
        System.out.println(FilenameUtils.getExtension("https://static.qker.com/aaa.png"));
    }
}
