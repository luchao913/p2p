package net.p2pcdn.util;

import org.apache.commons.codec.binary.Hex;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * 类名称：MD5Util
 * 类描述：
 * 创建人：chenbo
 * 修改人：chenbo
 * 修改时间：2015年2月9日 下午1:52:06
 * 修改备注：
 * @version 1.0.0
 *
 */
public class MD5Util {
	
	public static void main(String[] args) {
	}

	/**
	 * 
	 * @param plain  明文
	 * @return 32位小写密文
	 */
	public static String encryption(String plain) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(plain.getBytes());
			return  Hex.encodeHexString(md.digest());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}
}
