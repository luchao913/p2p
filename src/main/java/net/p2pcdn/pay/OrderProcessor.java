package net.p2pcdn.pay;

import net.p2pcdn.core.order.domain.OrderDetail;
import net.p2pcdn.core.product.domain.ProductType;

public interface OrderProcessor {
    void process(int buyer, OrderDetail orderDetail);
    ProductType getSupportType();
}
