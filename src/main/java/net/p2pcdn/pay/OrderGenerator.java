package net.p2pcdn.pay;

import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;
import net.p2pcdn.core.product.domain.ProductType;
import net.p2pcdn.pay.paypal.config.CreatePaymentCommand;
import org.springframework.beans.factory.InitializingBean;

public interface OrderGenerator extends InitializingBean {
    Payment createPayment(CreatePaymentCommand createPaymentCommand) throws PayPalRESTException;

    ProductType getProductType();
}
