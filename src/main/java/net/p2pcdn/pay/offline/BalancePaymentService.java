package net.p2pcdn.pay.offline;

import com.qc.bean.result.ResponseData;
import net.p2pcdn.core.order.domain.Order;
import net.p2pcdn.core.order.domain.PayType;
import net.p2pcdn.core.order.domain.PlatformEnum;
import net.p2pcdn.core.order.service.OrderService;
import net.p2pcdn.core.product.domain.Product;
import net.p2pcdn.core.product.service.ProductService;
import net.p2pcdn.pay.OrderProcessorFactory;
import net.p2pcdn.user.domain.UserAccount;
import net.p2pcdn.user.domain.UserAccountChangeRecord;
import net.p2pcdn.user.service.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @author zx
 */
@Service
public class BalancePaymentService {
    @Autowired
    private OrderService orderService;
    @Autowired
    private UserAccountService userAccountService;
    @Autowired
    private ProductService productService;
    @Autowired
    private OrderProcessorFactory orderProcessorFactory;

    public ResponseData purchaseTrafficPackage(int userId, int productId, int quantity, String domains) {
        ResponseData responseData = checkBalanceEnough(userId, productId, quantity);
        if (!responseData.isSuccess()) {
            return responseData;
        }
        Order order = orderService.createPackageOrder(userId, productId, quantity, null, PayType.ONLINE, PlatformEnum.BALANCE, domains);
        orderProcessorFactory.process(null, null, order.getTradeNo());
        Product product = productService.getProduct(productId);
        userAccountService.expense(userId, order.getActualPayment(), UserAccountChangeRecord.BizType.EXPENSE, "余额购买产品：" + product.getSerial(), order.getId());
        return ResponseData.success("ok");
    }


    public ResponseData purchaseBags(int userId,int contractId, int productId, int quantity) {
        ResponseData responseData = checkBalanceEnough(userId, productId, quantity);
        if (!responseData.isSuccess()) {
            return responseData;
        }
        Order order = orderService.createTrafficOrder(userId, productId, quantity, null, PayType.ONLINE, PlatformEnum.BALANCE,contractId);
        orderProcessorFactory.process(null, null, order.getTradeNo());
        Product product = productService.getProduct(productId);
        userAccountService.expense(userId, order.getActualPayment(), UserAccountChangeRecord.BizType.EXPENSE, "余额购买流量包：" + product.getSerial(), order.getId());
        return ResponseData.success("ok");
    }



    public ResponseData checkBalanceEnough(int userId, int productId, int quantity) {
        Product product = productService.getProduct(productId);
        if (product.getStatus() == Product.UN_PUBLISHED) {
            return ResponseData.error("产品已下架，不能购买", 500);
        }
        UserAccount userAccount = userAccountService.getUserAccount(userId);
        int value = userAccount.getBalance().compareTo(product.getPrice().multiply(new BigDecimal(quantity)));
        if (value > 0) {
            return ResponseData.error("账户余额不足", 500);
        }
        return ResponseData.success("ok");
    }
}
