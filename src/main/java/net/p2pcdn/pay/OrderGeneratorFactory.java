package net.p2pcdn.pay;

import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;
import net.p2pcdn.core.product.domain.ProductType;
import net.p2pcdn.pay.paypal.config.CreatePaymentCommand;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class OrderGeneratorFactory {
    private Map<ProductType, OrderGenerator> orderGeneratorMap = new ConcurrentHashMap<>();

    public void addGenerator(ProductType productType, OrderGenerator orderGenerator) {
        this.orderGeneratorMap.put(productType, orderGenerator);
    }

    public Payment createPayment(CreatePaymentCommand command) throws PayPalRESTException {
        return orderGeneratorMap.get(command.getProductType()).createPayment(command);
    }
}
