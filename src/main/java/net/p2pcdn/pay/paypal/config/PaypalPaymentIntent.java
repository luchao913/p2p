package net.p2pcdn.pay.paypal.config;
 
public enum PaypalPaymentIntent {
    sale, authorize, order
}