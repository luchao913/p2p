package net.p2pcdn.pay.paypal.config;

import lombok.Getter;
import net.p2pcdn.core.product.domain.ProductType;

@Getter
public class CreatePaymentCommand {
    private int userId;
    private int itemId;
    private int quantity;
    private Integer contractId;
    private String amountOfMoney;
    private String currency;
    private String domains;
    private PaypalPaymentMethod paymentMethod;
    private PaypalPaymentIntent paypalPaymentIntent;
    private String successUrl;
    private String cancelUrl;
    private String notifyUrl;
    private ProductType productType;

    public CreatePaymentCommand setDomains(String domains) {
        this.domains = domains;
        return this;
    }

    public CreatePaymentCommand setContractId(Integer contractId) {
        this.contractId = contractId;
        return this;
    }

    public CreatePaymentCommand setProductType(ProductType productType) {
        this.productType = productType;
        return this;
    }

    public CreatePaymentCommand setAmountOfMoney(String amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
        return this;
    }

    public CreatePaymentCommand setSuccessUrl(String successUrl) {
        this.successUrl = successUrl;
        return this;
    }

    public CreatePaymentCommand setCancelUrl(String cancelUrl) {
        this.cancelUrl = cancelUrl;
        return this;
    }

    public CreatePaymentCommand setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
        return this;
    }

    public CreatePaymentCommand setUserId(int userId) {
        this.userId = userId;
        return this;
    }

    public CreatePaymentCommand setItemId(int itemId) {
        this.itemId = itemId;
        return this;
    }

    public CreatePaymentCommand setQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    public CreatePaymentCommand setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public CreatePaymentCommand setPaymentMethod(PaypalPaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
        return this;
    }

    public CreatePaymentCommand setPaypalPaymentIntent(PaypalPaymentIntent paypalPaymentIntent) {
        this.paypalPaymentIntent = paypalPaymentIntent;
        return this;
    }
}
