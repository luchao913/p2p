package net.p2pcdn.pay.paypal.config;
 
public enum PaypalPaymentMethod {
    credit_card, paypal
}