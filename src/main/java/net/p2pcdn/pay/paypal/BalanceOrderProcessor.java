package net.p2pcdn.pay.paypal;

import net.p2pcdn.core.order.domain.OrderDetail;
import net.p2pcdn.core.order.service.OrderService;
import net.p2pcdn.core.product.domain.ProductType;
import net.p2pcdn.pay.OrderProcessor;
import net.p2pcdn.pay.OrderProcessorFactory;
import net.p2pcdn.user.domain.UserAccountChangeRecord;
import net.p2pcdn.user.service.UserAccountService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author zx
 */
@Service
public class BalanceOrderProcessor implements OrderProcessor, InitializingBean {
    @Autowired
    private OrderService orderService;
    @Autowired
    private UserAccountService userAccountService;
    @Autowired
    private OrderProcessorFactory orderProcessorFactory;

    @Override
    public void process(int buyer, OrderDetail orderDetail) {
        userAccountService.chargeBalance(buyer, buyer, orderDetail.getPrice(), UserAccountChangeRecord.BizType.ON_LINE_PURCHASE, "线上自费购买");
    }

    @Override
    public ProductType getSupportType() {
        return ProductType.BALANCE;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        orderProcessorFactory.addProcessor(getSupportType(), this);
    }
}
