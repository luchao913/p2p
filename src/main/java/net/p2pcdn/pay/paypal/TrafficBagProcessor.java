package net.p2pcdn.pay.paypal;

import net.p2pcdn.core.order.domain.OrderDetail;
import net.p2pcdn.core.order.service.OrderHandlerFactory;
import net.p2pcdn.core.product.domain.ProductType;
import net.p2pcdn.pay.OrderProcessor;
import net.p2pcdn.pay.OrderProcessorFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrafficBagProcessor implements OrderProcessor, InitializingBean {
    @Autowired
    private OrderProcessorFactory orderProcessorFactory;
    @Autowired
    private OrderHandlerFactory orderHandlerFactory;
    
    @Override
    public void process(int buyer, OrderDetail orderDetail) {
        orderHandlerFactory.handle(getSupportType(), orderDetail);
    }

    @Override
    public ProductType getSupportType() {
        return ProductType.BAG;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        orderProcessorFactory.addProcessor(getSupportType(),this);
    }
}
