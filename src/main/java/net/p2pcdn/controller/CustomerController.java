package net.p2pcdn.controller;

import com.qc.bean.result.ResponseData;
import net.p2pcdn.core.order.command.ContractQueryCommand;
import net.p2pcdn.core.order.command.QueryTrafficCommand;
import net.p2pcdn.core.order.domain.Contract;
import net.p2pcdn.core.order.service.CDNConsumeRecordService;
import net.p2pcdn.core.order.service.ContractService;
import net.p2pcdn.core.order.service.OrderService;
import net.p2pcdn.core.product.domain.Product;
import net.p2pcdn.core.product.domain.ProductType;
import net.p2pcdn.core.product.service.ProductService;
import net.p2pcdn.user.domain.User;
import net.p2pcdn.user.domain.UserAccountChangeRecord;
import net.p2pcdn.user.service.UserAccountService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private UserAccountService userAccountService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ContractService contractService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private CDNConsumeRecordService cdnConsumeRecordService;

    @GetMapping("balance/flow")
    public ResponseData queryChangeRecords(int page, int size, String bizType) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        if (user == null) {
            return ResponseData.error("您会登录或会话已过期", 403);
        }
        return ResponseData.success(userAccountService.queryChangeRecords(null, user.getId(), null, StringUtils.isNotBlank(bizType) ?
                        UserAccountChangeRecord.BizType.valueOf(bizType) : null
                , page, size));
    }

    @GetMapping("product/q/1")
    public ResponseData query(Integer type, int page, int size) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        if (user == null) {
            return ResponseData.error("您会登录或会话已过期", 403);
        }
        return ResponseData.success(productService.query(null, null, Product.PUBLISHED, type == null ? ProductType.PACKAGE.getValue() : type, page, size));
    }

    @GetMapping("contract/q")
    public ResponseData queryContracts(ContractQueryCommand command) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        if (user == null) {
            return ResponseData.error("您会登录或会话已过期", 403);
        }
        command.setUserId(command.getUserId());
        command.setStatus(Contract.EFFECTIVE);
        return ResponseData.success(contractService.queryContracts(command));
    }


    @GetMapping("contract/{id}/traffic/records")
    public ResponseData findByContractId(@PathVariable("id") int contractId, QueryTrafficCommand command) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        if (user == null) {
            return ResponseData.error("您会登录或会话已过期", 403);
        }
        command.setContractId(contractId);
        Contract contract = contractService.getById(contractId);
        if (contract.getUserId() != user.getId()) {
            return ResponseData.error("not found", 403);
        }
        return ResponseData.success(contractService.findByContractId(command));
    }

    @GetMapping("contract/{id}/traffic/details")
    public ResponseData queryTrafficDetails(@PathVariable("id") int contractId, int page, int size, String from, String to, Boolean done, String domain) throws ParseException {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        if (user == null) {
            return ResponseData.error("您会登录或会话已过期", 403);
        }
        Contract contract = contractService.getById(contractId);
        if (contract.getUserId() != user.getId()) {
            return ResponseData.error("not found", 403);
        }
        return ResponseData.success(orderService.queryTrafficDetails(page, size, contractId, from, to, done, domain));
    }

    @GetMapping("p2phitrate")
    public ResponseData getP2PTrafficHitRate(String from, String to) throws ParseException {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        if (user == null) {
            return ResponseData.error("您会登录或会话已过期", 403);
        }
        return ResponseData.success(cdnConsumeRecordService.getP2pTrafficHitRate(user.getId(), from, to));
    }

    @GetMapping("usingrecords")
    public ResponseData queryContractUsingRecords(int page, int size, Boolean expired, Integer status) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        if (user == null) {
            return ResponseData.error("您会登录或会话已过期", 403);
        }
        return ResponseData.success(contractService.queryContractUsingRecords((page - 1) * size, size, user.getId(), expired, status));
    }
}
