package net.p2pcdn.controller;

import com.qc.bean.result.ResponseData;
import net.p2pcdn.core.order.domain.CDNConsumeRecord;
import net.p2pcdn.core.order.domain.UploadVO;
import net.p2pcdn.core.order.service.CDNConsumeRecordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zx
 */
@RestController
@RequestMapping("/upload")
public class UploadController {
    private final Logger _logger = LoggerFactory.getLogger(UploadController.class);
    @Autowired
    private CDNConsumeRecordService cdnConsumeRecordService;

    @PostMapping(value = "cdn/consumeRecord",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseData accept(@RequestBody List<UploadVO> records) {
        if(records.isEmpty()){
            return ResponseData.error("http request body is empty",500);
        }
        _logger.info("==========================================================");
        _logger.info(records.toString());
        _logger.info("==========================================================");
        List<CDNConsumeRecord> recordList = new ArrayList<>();
        records.forEach(item -> recordList.add(UploadVO.build(item)));
        cdnConsumeRecordService.saveCDNConsumeRecord(recordList);
        return ResponseData.success("ok");
    }

    @GetMapping("/whitelist")
    public ResponseData getWhiteListDomain(){
        return ResponseData.success(cdnConsumeRecordService.getEffectiveDomains());
    }
}
