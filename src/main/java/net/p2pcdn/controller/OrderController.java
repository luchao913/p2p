package net.p2pcdn.controller;


import com.qc.bean.result.ResponseData;
import net.p2pcdn.core.order.command.ContractQueryCommand;
import net.p2pcdn.core.order.command.QueryTrafficCommand;
import net.p2pcdn.core.order.domain.OrderQueryCommand;
import net.p2pcdn.core.order.service.ContractService;
import net.p2pcdn.core.order.service.OrderService;
import net.p2pcdn.user.domain.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.ParseException;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;
    @Autowired
    private ContractService contractService;

    @GetMapping("/q")
    @RequiresRoles(value = {"ROLE_ADMIN", "ROLE_SUPER_ADMIN"}, logical = Logical.OR)
    public ResponseData query(OrderQueryCommand command) throws ParseException {
        return ResponseData.success(orderService.queryOrder(command));
    }

    @GetMapping("/bi/daily")
    @RequiresRoles(value = {"ROLE_ADMIN", "ROLE_SUPER_ADMIN"}, logical = Logical.OR)
    public ResponseData groupByDailyIncome(String from, String to) {
        return ResponseData.success(orderService.groupByDailyIncome(from, to));
    }


    @GetMapping("bi/month")
    @RequiresRoles(value = {"ROLE_ADMIN", "ROLE_SUPER_ADMIN"}, logical = Logical.OR)
    public ResponseData groupByMonthIncome(String from, String to) {
        return ResponseData.success(orderService.groupByMonthIncome(from, to));
    }

    @GetMapping("bi/data")
    @RequiresRoles(value = {"ROLE_ADMIN", "ROLE_SUPER_ADMIN"}, logical = Logical.OR)
    public ResponseData getOperationData() {
        return ResponseData.success(orderService.getOperationData());
    }

    @GetMapping("contract/q")
    @RequiresRoles(value = {"ROLE_ADMIN", "ROLE_SUPER_ADMIN"}, logical = Logical.OR)
    public ResponseData queryContracts(ContractQueryCommand command) {
        return ResponseData.success(contractService.queryContracts(command));
    }

    @PostMapping("contract/cancel")
    @RequiresRoles(value = {"ROLE_ADMIN", "ROLE_SUPER_ADMIN"}, logical = Logical.OR)
    public ResponseData expireTime(int id) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        if (user == null) {
            return ResponseData.error("您会登录或会话已过期", 403);
        }
        contractService.cancelContract(user.getId(), id);
        return ResponseData.success("ok");
    }

    @GetMapping("contract/{id}")
    @RequiresRoles(value = {"ROLE_ADMIN", "ROLE_SUPER_ADMIN"}, logical = Logical.OR)
    public ResponseData queryContracts(@PathVariable("id") int contractId) {
        return ResponseData.success(contractService.getContactVO(contractId));
    }

    @GetMapping("contract/{id}/traffic/records")
    @RequiresRoles(value = {"ROLE_ADMIN", "ROLE_SUPER_ADMIN"}, logical = Logical.OR)
    public ResponseData findByContractId(@PathVariable("id") int contractId, QueryTrafficCommand command) {
        command.setContractId(contractId);
        return ResponseData.success(contractService.findByContractId(command));
    }

    @GetMapping("contract/{id}/traffic/details")
    @RequiresRoles(value = {"ROLE_ADMIN", "ROLE_SUPER_ADMIN"}, logical = Logical.OR)
    public ResponseData queryTrafficDetails(@PathVariable("id") int contractId, int page, int size, String from, String to, Boolean done, String domain) throws ParseException {
        return ResponseData.success(orderService.queryTrafficDetails(page,size,contractId,from,to,done,domain));
    }
}
