package net.p2pcdn.task;

import net.p2pcdn.common.Date2StringConverter;
import net.p2pcdn.core.order.domain.WillEffectiveContract;
import net.p2pcdn.core.order.repository.ContractRepository;
import net.p2pcdn.core.order.repository.WillEffectiveContractRepository;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 每分钟 扫描一次
 *
 * @author zx
 */
@Component
public class EffectiveContractExecutor {
    @Resource
    private WillEffectiveContractRepository willEffectiveContractRepository;
    @Resource
    private ContractRepository contractRepository;

    @Scheduled(cron = "0 */1 * * * ?")
    public void execute() {
        Map<String, Object> params = new HashMap<>(7);
        params.put("from", DateUtils.addMinutes(new Date(), -5));
        params.put("to", DateUtils.addMinutes(new Date(), 5));
        List<WillEffectiveContract> contractList = willEffectiveContractRepository.query(params);
        if (!contractList.isEmpty()) {
            contractRepository.effectiveContracts(contractList.stream().map(WillEffectiveContract::getContractId).collect(Collectors.toList()));
            willEffectiveContractRepository.done(contractList);
        }
    }
}
