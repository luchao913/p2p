package net.p2pcdn.task;

import net.p2pcdn.common.Date2StringConverter;
import net.p2pcdn.core.order.domain.ContractDO;
import net.p2pcdn.core.order.domain.ExpiringContract;
import net.p2pcdn.core.order.repository.ContractRepository;
import net.p2pcdn.core.order.repository.ExpiringContractRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;

/**
 * 处理即将时效的合约
 *
 * @author zx
 */
@Component
public class ExpiringContractExecutor {
    @Resource
    private ExpiringContractRepository expiringContractRepository;
    @Autowired
    private ContractRepository contractRepository;

    @Scheduled(cron = "0 0 0 */1 * ?")
    public void execute(){
        Map<String,Object> params = new HashMap<>(5);
        Date from = Date2StringConverter.getDateOfStartTime();
        Date to  = Date2StringConverter.getDateOfEndTime();
        params.put("from",from);
        params.put("to",to);
        List<ContractDO> contracts = null;
        do{
            contracts  = contractRepository.queryExpiringContracts(params);
            List<ExpiringContract> expiringContracts = new ArrayList<>();
            contracts.forEach(item -> expiringContracts.add(new ExpiringContract(item)));
            if(expiringContracts.size() > 0){
                expiringContractRepository.batchInsert(expiringContracts);
            }
        }
        while(contracts.size() > 0);
    }


}
