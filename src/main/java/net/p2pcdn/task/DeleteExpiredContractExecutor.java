package net.p2pcdn.task;

import net.p2pcdn.core.order.repository.ContractRepository;
import net.p2pcdn.core.order.repository.ExpiringContractRepository;
import net.p2pcdn.core.order.repository.WillEffectiveContractRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 *
 * 每分钟 扫描一次
 * 删除已处理的合约记录
 * @author zx
 */
@Component
public class DeleteExpiredContractExecutor {
    @Resource
    private ExpiringContractRepository expiringContractRepository;
    @Resource
    private ContractRepository contractRepository;

    @Scheduled(cron = "0 0 1 * * ?")
    public void execute() {
        expiringContractRepository.deleteExpiredContracts();
    }
}
