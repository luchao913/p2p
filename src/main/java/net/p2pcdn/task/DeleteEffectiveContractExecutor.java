package net.p2pcdn.task;

import net.p2pcdn.core.order.repository.ContractRepository;
import net.p2pcdn.core.order.repository.WillEffectiveContractRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 每分钟 扫描一次
 *
 * @author zx
 */
@Component
public class DeleteEffectiveContractExecutor {
    @Resource
    private WillEffectiveContractRepository willEffectiveContractRepository;
    @Resource
    private ContractRepository contractRepository;
    
    @Scheduled(cron = "0 0 1 * * ?")
    public void execute() {
        willEffectiveContractRepository.deleteExpiredContracts();
    }
}
