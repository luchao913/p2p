package net.p2pcdn.realm;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class RolesAuthorizationFilter extends AuthorizationFilter{  
  
    @Override  
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        Subject subject = getSubject(request, response);   
        String[] rolesArray = (String[]) mappedValue;   
        if (rolesArray == null || rolesArray.length == 0) {   
            return true;   
        }   
        for(String role : rolesArray){
            if(subject.hasRole(role)){
                return true;    
            }
        }
        return false;    
    }  
}