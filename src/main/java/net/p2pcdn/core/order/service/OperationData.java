package net.p2pcdn.core.order.service;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
public class OperationData {
    private BigDecimal userCount;
    private BigDecimal orderCount;
    private BigDecimal orderCash;
    private BigDecimal domainCount;
    private BigDecimal effectiveCount;
    private BigDecimal unEffectiveCount;


    public OperationData(List<BigDecimal> data){
        this.userCount = data.get(0);
        this.orderCount = data.get(1);
        this.orderCash = data.get(2);
        this.domainCount = data.get(3);
        this.effectiveCount = data.get(4);
        this.unEffectiveCount = data.get(5);
    }

}
