package net.p2pcdn.core.order.service;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
public class Task {
    private String domain;
    private BigDecimal traffic;
    private List<String> recordIds;

    public Task(String domain, BigDecimal traffic, List<String> recordIds) {
        this.domain = domain;
        this.traffic = traffic;
        this.recordIds = recordIds;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
