package net.p2pcdn.core.order.service;

import net.p2pcdn.core.order.domain.OrderDetail;
import net.p2pcdn.core.order.domain.PayType;
import net.p2pcdn.core.order.domain.TrafficUpdateRecord;
import net.p2pcdn.core.product.domain.ProductType;
import org.springframework.beans.factory.InitializingBean;

public interface OrderHandler extends InitializingBean {
    ProductType supportType();

    void handle(OrderDetail detail);

    default TrafficUpdateRecord.ChangeType getChangeType(int payType) {
        if (payType == PayType.OFFLINE.ordinal()) {
            return TrafficUpdateRecord.ChangeType.ADMIN_ADD;
        }
        if (payType == PayType.ONLINE.ordinal()) {
            return TrafficUpdateRecord.ChangeType.ON_LINE_PURCHASE;
        }
        if (payType == PayType.BALANCE.ordinal()) {
            return TrafficUpdateRecord.ChangeType.BALANCE_PURCHASE;
        }
        throw new RuntimeException("parameter payType is illegal");
    }
}
