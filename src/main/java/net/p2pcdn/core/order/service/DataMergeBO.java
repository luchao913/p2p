package net.p2pcdn.core.order.service;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
public class DataMergeBO {
    private Map<String, List<String>> domainRelatedRecordIds;
    private Map<String, Double> domainRelatedTraffic;

    public DataMergeBO(Map<String, List<String>> ids, Map<String, Double> trafficMap) {
        this.domainRelatedRecordIds = ids;
        this.domainRelatedTraffic = trafficMap;
    }
}
