package net.p2pcdn.core.order.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * @author zx
 */
@Getter
@Setter
@NoArgsConstructor
public class P2PTrafficHitRateVO {
    private BigDecimal p2pDownloaded;
    private BigDecimal httpDownloaded;
    private BigDecimal todayP2PDownloaded;
    private BigDecimal todayHttpDownloaded;


    protected P2PTrafficHitRateVO(List<String> values) {
        String raw1 = values.get(0);
        if (StringUtils.isBlank(raw1)) {
            p2pDownloaded = new BigDecimal("0");
        } else {
            p2pDownloaded = new BigDecimal(raw1).divide(new BigDecimal(1024), 4, RoundingMode.HALF_UP);
        }
        String raw2 = values.get(1);
        if (StringUtils.isBlank(raw2)) {
            httpDownloaded = new BigDecimal("0");
        } else {
            httpDownloaded = new BigDecimal(raw2).divide(new BigDecimal(1024), 4, RoundingMode.HALF_UP);
        }
        String raw3 = values.get(2);
        if (StringUtils.isBlank(raw2)) {
            todayP2PDownloaded = new BigDecimal("0");
        } else {
            todayP2PDownloaded = new BigDecimal(raw3).divide(new BigDecimal(1024), 4, RoundingMode.HALF_UP);
        }
        String raw4 = values.get(3);
        if (StringUtils.isBlank(raw2)) {
            todayHttpDownloaded = new BigDecimal("0");
        } else {
            todayHttpDownloaded = new BigDecimal(raw4).divide(new BigDecimal(1024), 4, RoundingMode.HALF_UP);
        }
    }

    public BigDecimal getTodayHitRate() {
        BigDecimal zero = new BigDecimal("0");
        if (todayHttpDownloaded.compareTo(zero) == 0 && todayP2PDownloaded.compareTo(zero) == 0) {
            return zero;
        } else {
            return todayP2PDownloaded.divide(todayHttpDownloaded.add(todayP2PDownloaded), 4, RoundingMode.HALF_UP);
        }
    }

    public BigDecimal getHitRate() {
        BigDecimal zero = new BigDecimal("0");
        if (httpDownloaded.compareTo(zero) == 0 && p2pDownloaded.compareTo(zero) == 0) {
            return zero;
        } else {
            return p2pDownloaded.divide(httpDownloaded.add(p2pDownloaded), 4, RoundingMode.HALF_UP);
        }
    }
}
