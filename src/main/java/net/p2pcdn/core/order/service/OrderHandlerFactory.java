package net.p2pcdn.core.order.service;

import net.p2pcdn.core.order.domain.OrderDetail;
import net.p2pcdn.core.product.domain.ProductType;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 订单处理工厂类
 * @author zx
 */
@Service
public class OrderHandlerFactory {
    private static final Map<ProductType, OrderHandler> handlerMap = new ConcurrentHashMap<>();

    public void handle(ProductType productType, OrderDetail orderDetail) {
        OrderHandler orderHandler = handlerMap.get(productType);
        if (orderHandler == null) {
            throw new RuntimeException("OrderHandler Not Found");
        }
        orderHandler.handle(orderDetail);
    }

    public void addHandler(ProductType productType, OrderHandler handler) {
        OrderHandlerFactory.handlerMap.put(productType, handler);
    }
}
