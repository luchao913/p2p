package net.p2pcdn.core.order.command;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zx
 */
@Getter
@Setter
public class QueryTrafficCommand {
    private Integer contractId;
    private Integer changeType;
    private Boolean add;
    private Integer userId;
    private String from;
    private String to;
    private int page;
    private int size;
}
