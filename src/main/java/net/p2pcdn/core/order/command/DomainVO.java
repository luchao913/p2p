package net.p2pcdn.core.order.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author zx
 */
@Getter
@Setter
@NoArgsConstructor
public class DomainVO {
    private Integer id;
    private String domain;

    public DomainVO(int id,String domain){
        this.id = id;
        this.domain = domain;
    }
}
