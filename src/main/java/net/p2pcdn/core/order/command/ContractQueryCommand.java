package net.p2pcdn.core.order.command;

import lombok.*;

/**
 * @author zx
 */
@Getter
@Setter
@NoArgsConstructor
public class ContractQueryCommand {
    private Integer userId;
    private Integer productId;
    private Boolean expired;
    private Integer status;
    private Integer type;
    private String domain;
    private String email;
    private int page;
    private int size;
}
