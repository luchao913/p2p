package net.p2pcdn.core.order.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Administrator
 */
@Getter
@Setter
public class ContractUsingVO {
    private Integer id;
    private Integer productId;
    private String serial;
    private String productName;
    private BigDecimal remainingQuantity;
    private BigDecimal standardQuantity;
    private BigDecimal extraQuantity;
    private Date expireTime;
    private Integer status;
    private Integer type;
    private String domains;
    private Integer orderId;
    private BigDecimal http;
    private BigDecimal p2p;
}
