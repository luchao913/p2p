package net.p2pcdn.core.order.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

/**
 * 即将生效的合约
 * @author zx
 */
@Getter
@Setter
@NoArgsConstructor
public class WillEffectiveContract {
    private String id;

    private int contractId;
    private Date startTime;
    private boolean done;

    public WillEffectiveContract(ContractDO contractDO){
        this.id = UUID.randomUUID().toString();
        this.contractId = contractDO.getId();
        this.startTime = contractDO.getTime();
    }
}
