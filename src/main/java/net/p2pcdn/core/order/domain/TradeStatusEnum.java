package net.p2pcdn.core.order.domain;

public enum TradeStatusEnum {
    WAIT_BUYER_PAY(1),
    TRADE_SUCCESS(2),
    CANCEL(0),
    REFUND(-1);

    public final int value;

    TradeStatusEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
