package net.p2pcdn.core.order.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CDNDomain {
    private int id;
    private int contractId;
    private String domain;

    public CDNDomain(int contractId, String domain) {
        this.contractId = contractId;
        this.domain = domain;
    }
}
