package net.p2pcdn.core.order.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.p2pcdn.core.product.domain.Product;
import org.springframework.util.Assert;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class OrderDetail {
    private int id;
    private int orderId;
    private int itemId;
    private int itemType;
    private String name;
    private BigDecimal price;
    private int num;
    private String domains;
    private Integer contractId;


    protected OrderDetail(int orderId, Product product, String domains,int num) {
        Assert.notNull(product, "购买的产品对象不能为空");
        this.orderId = orderId;
        this.itemId = product.getId();
        this.itemType = product.getType();
        this.name = product.getName();
        this.price = product.getPrice();
        this.num = num;
        this.domains = domains;
    }

    protected OrderDetail(int orderId, Product product, Integer contractId,int num) {
        Assert.notNull(product, "购买的产品对象不能为空");
        this.orderId = orderId;
        this.itemId = product.getId();
        this.itemType = product.getType();
        this.name = product.getName();
        this.price = product.getPrice();
        this.num = num;
        this.contractId = contractId;
    }
}
