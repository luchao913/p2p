package net.p2pcdn.core.order.domain;

public enum PayType {
    /**
     * 线上支付
     */
    ONLINE,
    /**
     * 线下支付
     */
    OFFLINE,
    /**
     * 余额支付
     */
    BALANCE
}
