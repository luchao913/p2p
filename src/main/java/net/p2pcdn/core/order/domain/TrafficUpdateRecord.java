package net.p2pcdn.core.order.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 流量更新记录
 */
@Getter
@Setter
@NoArgsConstructor
public class TrafficUpdateRecord {
    private int id;
    private int contractId;
    private Date createTime;
    private BigDecimal amountOfTraffic;
    //流量包增加类型
    private int changeType;
    private boolean add;
    private int userId;
    private String username;//vo properties

    public TrafficUpdateRecord(int contractId, int userId, BigDecimal amount, ChangeType changeType, boolean add) {
        this.contractId = contractId;
        this.userId = userId;
        this.amountOfTraffic = amount;
        this.changeType = changeType.getValue();
        this.add = add;
        this.createTime = new Date();
    }

    public static enum ChangeType {
        /**
         * 在线购买流量包
         */
        ON_LINE_PURCHASE(1),
        /**
         * 余额购买流量包
         */
        BALANCE_PURCHASE(2),
        /**
         * 管理员增加
         */
        ADMIN_ADD(3),
        /**
         * 旧服务余量转入
         */
        IMPORT(4),
        /**
         * 旧服务余量转出
         */
        EXPORT(5),
        /**
         * 消费
         */
        CONSUME(6);
        private final int value;

        ChangeType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}
