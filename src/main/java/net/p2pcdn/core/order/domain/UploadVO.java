package net.p2pcdn.core.order.domain;


import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Setter
public class UploadVO implements Serializable {
    private final String ERROR_MARK = "NaN";
    private String http_download_speed;
    private String http_downloaded;
    private String p2p_download_percent;
    private String p2p_download_speed;//速度
    private String p2p_downloaded_percent;//占比
    private String p2p_downloaded;//流量
    private String upload_speed;
    private String uploaded;
    private String url;
    private long timestamp;

    public long getTimestamp(){
        return timestamp;
    }

    public String getHttp_download_speed() {
        if(StringUtils.isNotBlank(http_download_speed) && !ERROR_MARK.equals(http_download_speed)){
            return http_download_speed;
        }
        return "0";
    }

    public String getHttp_downloaded() {
        if(StringUtils.isNotBlank(http_downloaded) && !ERROR_MARK.equals(http_downloaded)){
            return http_downloaded;
        }
        return "0";
    }

    public String getP2p_download_percent() {
        if(StringUtils.isNotBlank(p2p_download_percent) && !ERROR_MARK.equals(p2p_download_percent)){
            return p2p_download_percent;
        }
        return "0";
    }

    public String getP2p_download_speed() {
        if(StringUtils.isNotBlank(p2p_download_speed) && !ERROR_MARK.equals(p2p_download_speed)){
            return p2p_download_speed;
        }
        return "0";
    }

    public String getP2p_downloaded_percent() {
        if(StringUtils.isNotBlank(p2p_downloaded_percent) && !ERROR_MARK.equals(p2p_downloaded_percent)){
            return p2p_downloaded_percent;
        }
        return "0";
    }

    public String getP2p_downloaded() {
        if(StringUtils.isNotBlank(p2p_downloaded) && !ERROR_MARK.equals(p2p_downloaded)){
            return p2p_downloaded;
        }
        return "0";
    }

    public String getUpload_speed() {
        return upload_speed;
    }

    public String getUploaded() {
        if(StringUtils.isNotBlank(uploaded) && !ERROR_MARK.equals(uploaded)){
            return uploaded;
        }
        return "0";
    }

    public String getUrl() {
        return url;
    }

    public static CDNConsumeRecord build(UploadVO vo){
        CDNConsumeRecord cdnConsumeRecord = new CDNConsumeRecord();
        cdnConsumeRecord.setHttp_download_speed(Double.parseDouble(vo.getHttp_download_speed()));
        cdnConsumeRecord.setHttp_downloaded(Double.parseDouble(vo.getHttp_downloaded()));
        cdnConsumeRecord.setP2p_download_percent(Double.parseDouble(vo.getP2p_download_percent()));
        cdnConsumeRecord.setP2p_download_speed(Double.parseDouble(vo.getP2p_download_speed()));
        cdnConsumeRecord.setP2p_downloaded(Double.parseDouble(vo.getP2p_downloaded()));
        cdnConsumeRecord.setP2p_downloaded_percent(Double.parseDouble(vo.getP2p_downloaded_percent()));
        cdnConsumeRecord.setUpload_speed(vo.getUpload_speed());
        cdnConsumeRecord.setUploaded(Double.parseDouble(vo.getUploaded()));
        cdnConsumeRecord.setUrl(vo.getUrl());
        cdnConsumeRecord.setCreateTime(new Date(vo.getTimestamp()));
        return cdnConsumeRecord;
    }
}
