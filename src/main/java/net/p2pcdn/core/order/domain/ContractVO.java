package net.p2pcdn.core.order.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContractVO extends  Contract {
    //vo properties
    private String productSerial;
    //vo properties
    private int productId;
    //vo properties
    private String productName;
    private String modifyName;
    private String username;
    private String cdnDomains;
}
