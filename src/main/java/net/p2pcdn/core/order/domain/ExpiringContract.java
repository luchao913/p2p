package net.p2pcdn.core.order.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

/**
 * 即将过期的合约
 * @author zx
 */
@Getter
@Setter
@NoArgsConstructor
public class ExpiringContract {
    private String id;
    private int contractId;
    private Date expireTime;
    private boolean done;

    public ExpiringContract(ContractDO contractDO){
        this.id = UUID.randomUUID().toString();
        this.contractId = contractDO.getId();
        this.expireTime = contractDO.getTime();
    }
}
