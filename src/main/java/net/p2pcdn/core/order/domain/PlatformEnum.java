package net.p2pcdn.core.order.domain;

/**
 * @author zx
 */

public enum PlatformEnum {
    //未知
    UNKNOWN(0),
    //公对公转账
    BANK(1),
    //支付宝
    ALIPAY(2),
    //微信
    WECHAT(3),

    PAYPAL(4),
    //余额
    BALANCE(5);

    private final int value;
    PlatformEnum(int value){
        this.value = value;
    }

    public int getValue(){
        return value;
    }
}
