package net.p2pcdn.core.order.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author zx
 */
@Getter
@Setter
public class ContractDO {
    private Integer id;
    private Date time;

    @Override
    public String toString() {
        return "ContractDO{" +
                "id=" + id +
                ", time=" + time +
                '}';
    }
}
