package net.p2pcdn.core.order.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author zx
 */
@Getter
@Setter
public class OrderVO extends Order {
    private String productId;
    private String productName;
    private String domains;
    private Integer contractStatus;
    private Integer contractType;
    private Date expireTime;
    private BigDecimal standardQuantity;
    private BigDecimal remainingQuantity;
    private BigDecimal extraQuantity;
    private String username;
}
