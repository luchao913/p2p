package net.p2pcdn.core.order.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.p2pcdn.core.product.domain.Product;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Order {
    private int id;
    private int buyer;//买方
    private Date payTime;//购买时间
    private int tradeStatus;//交易状态
    private String tradeNo;//交易编号
    private Date createTime;//创建时间
    private int payType;//付款方式
    private BigDecimal payment;//应付款
    private BigDecimal actualPayment;//实际付款
    private Boolean hasDiscount;//是否打折
    private int itemNum;//商品件数
    private int platform;//支付平台
    private String transactionId;
    private String remark;//备注
    private int creator;
    private String payerId;
    private OrderDetail orderDetail;


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    public Order(int buyer, String tradeNo, BigDecimal payment, BigDecimal actualPayment, PayType payType, PlatformEnum platformEnum, Integer creator,int itemNum) {
        Assert.notNull(payment, "应付金额不能为空");
        Assert.notNull(actualPayment, "实付金额不能为空");
        this.buyer = buyer;
        this.tradeNo = tradeNo;
        this.payment = payment;
        this.actualPayment = actualPayment;
        this.payType = payType.ordinal();
        this.platform = platformEnum.getValue();
        this.createTime = new Date();
        this.payTime = new Date();
        this.itemNum = itemNum;
        this.hasDiscount = actualPayment.compareTo(payment) < 0;
        this.tradeStatus = TradeStatusEnum.WAIT_BUYER_PAY.getValue();
        this.creator = creator == null ? buyer : creator;
    }

    public OrderDetail addItem(Product product, String domains) {
        OrderDetail orderDetail = new OrderDetail(this.id, product, domains,this.itemNum);
        this.setOrderDetail(orderDetail);
        return orderDetail;
    }

    public OrderDetail addItem(Product product, Integer contractId) {
        OrderDetail orderDetail = new OrderDetail(this.id, product, contractId,this.itemNum);
        this.setOrderDetail(orderDetail);
        return orderDetail;
    }
}
