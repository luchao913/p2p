package net.p2pcdn.core.order.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 *
 */
@Getter
@Setter
public class OrderQueryCommand {
    private Integer itemType;
    private String domain;
    private String email;
    private Integer payType;
    private Integer platform;
    private Integer tradeStatus;
    private String tradeNo;
    private Boolean expired;
    private String fromDate;
    private String toDate;
    private int page;
    private int size;
    private Date from;
    private Date to;
}
