package net.p2pcdn.core.order.domain;

import lombok.Getter;
import lombok.Setter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

/**
 * 第3方平台总动post的域名
 */
@Getter
@Setter
public class CDNConsumeRecord {
    private String id;
    private double http_download_speed;
    private double http_downloaded;
    private double p2p_download_percent;
    private double p2p_download_speed;//速度
    private double p2p_downloaded_percent;//占比
    private double p2p_downloaded;//流量
    private String upload_speed;
    private double uploaded;
    private String url;
    private String domain;
    private Date createTime;
    private boolean done;
    private Integer contractId;
    public String getDomain() {
        if (this.url.contains("http")) {
            URL url;
            try {
                url = new URL(this.url);
            } catch (MalformedURLException e) {
                throw new RuntimeException(e.getMessage());
            }
            return url.getHost();
        } else {
            return this.url;
        }
    }

    @Override
    public String toString() {
        return "CDNConsumeRecord{" +
                "id='" + id + '\'' +
                ", http_download_speed=" + http_download_speed +
                ", http_downloaded=" + http_downloaded +
                ", p2p_download_percent=" + p2p_download_percent +
                ", p2p_download_speed=" + p2p_download_speed +
                ", p2p_downloaded_percent=" + p2p_downloaded_percent +
                ", p2p_downloaded=" + p2p_downloaded +
                ", upload_speed='" + upload_speed + '\'' +
                ", uploaded=" + uploaded +
                ", url='" + url + '\'' +
                ", domain='" + domain + '\'' +
                ", createTime=" + createTime +
                ", done=" + done +
                '}';
    }
}

