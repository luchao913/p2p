package net.p2pcdn.core.order.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KeyValue {
    private String key;
    private String value;
}
