package net.p2pcdn.core.order.repository;

import net.p2pcdn.core.order.domain.CDNDomain;

import java.util.List;

public interface CDNDomainRepository {
    List<CDNDomain> findAll(List<Integer> contractIds);

    int batchInsert(List<CDNDomain> domains);

    List<CDNDomain> findByContractId(Integer oldContractId);

    Integer findContractId(String domain);

    List<String> findEffectiveDomains();
}
