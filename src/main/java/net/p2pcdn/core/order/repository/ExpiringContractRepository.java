package net.p2pcdn.core.order.repository;

import net.p2pcdn.core.order.domain.ExpiringContract;

import java.util.List;
import java.util.Map;

public interface ExpiringContractRepository {
    List<ExpiringContract> query(Map<String, Object> params);

    int batchInsert(List<ExpiringContract> contracts);

    int deleteExpiredContracts();

    int done(List<ExpiringContract> contractList);
}
