package net.p2pcdn.core.order.repository;

import net.p2pcdn.core.order.domain.CDNConsumeRecord;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface CDNConsumeRecordRepository {
    int batchInsert(List<CDNConsumeRecord> records);

    int done(@Param("list") List<String> recordIds,@Param("contractId") int contractId);

    List<CDNConsumeRecord> findUndoRecord(@Param("limit") int limit, @Param("from") Date from);

    List<CDNConsumeRecord> query(Map<String,Object> params);

    List<String> calcP2PTrafficRate(Map<String,Object> params);
}
