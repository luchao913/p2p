package net.p2pcdn.core.order.repository;

import net.p2pcdn.core.order.domain.OrderDetail;

public interface OrderDetailRepository {
    int insert(OrderDetail detail);

    OrderDetail selectByOrderId(int orderId);
}
