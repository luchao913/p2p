package net.p2pcdn.core.order.repository;

import net.p2pcdn.core.order.domain.TrafficUpdateRecord;
import net.p2pcdn.core.order.command.QueryTrafficCommand;

import java.util.List;

public interface TrafficUpdateRecordRepository {
    int insert(TrafficUpdateRecord trafficUpdateRecord);

    void batchInsert(List<TrafficUpdateRecord> records);

    List<TrafficUpdateRecord> findByContractId(QueryTrafficCommand command);
}
