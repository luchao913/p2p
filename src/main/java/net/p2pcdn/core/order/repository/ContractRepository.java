package net.p2pcdn.core.order.repository;

import net.p2pcdn.core.order.domain.Contract;
import net.p2pcdn.core.order.command.ContractQueryCommand;
import net.p2pcdn.core.order.domain.ContractDO;
import net.p2pcdn.core.order.domain.ContractUsingVO;
import net.p2pcdn.core.order.domain.ContractVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
public interface ContractRepository {
    List<Contract> findEffectiveContract(Map<String, Object> params);

    Contract selectByPrimaryKey(Integer integer);

    int insert(Contract newContract);

    int batchInsert(List<Contract> contracts);

    int updateByPrimaryKey(Contract oldContract);

    List<ContractVO> query(ContractQueryCommand contractQueryCommand);

    ContractVO fullSelect(int id);

    List<ContractUsingVO> queryContractUsingDetail(Map<String,Object> params);

    int countContractUsingDetail(Map<String,Object> params);

    Contract findLastUnEffectiveContract(@Param("userId") int userId,@Param("productId") int productId);

    List<ContractDO> queryExpiringContracts(Map<String,Object> params);

    List<ContractDO> queryContractsThatBecomeEffective(Map<String,Object> params);

    int expireContracts(List<Integer> ids);

    int effectiveContracts(List<Integer> ids);
}
