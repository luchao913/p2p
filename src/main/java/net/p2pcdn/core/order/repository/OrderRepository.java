package net.p2pcdn.core.order.repository;

import net.p2pcdn.core.order.domain.KeyValue;
import net.p2pcdn.core.order.domain.Order;
import net.p2pcdn.core.order.domain.OrderVO;
import net.p2pcdn.core.order.domain.OrderQueryCommand;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface OrderRepository {
    int insert(Order order);

    Order selectByPrimaryKey(int orderId);

    List<OrderVO> query(OrderQueryCommand command);

    int count(OrderQueryCommand command);

    List<KeyValue> groupByDailyIncome(Map<String, Object> params);

    List<KeyValue> groupByMonthIncome(Map<String, Object> params);

    List<BigDecimal> findOperationData();

    Order findByOrderNo(String orderNo);

    void updateByPrimaryKey(Order order);
}
