package net.p2pcdn.core.order.repository;

import net.p2pcdn.core.order.domain.WillEffectiveContract;

import java.util.List;
import java.util.Map;

public interface WillEffectiveContractRepository {
    List<WillEffectiveContract> query(Map<String, Object> params);

    int batchInsert(List<WillEffectiveContract> contracts);

    int deleteExpiredContracts();

    void done(List<WillEffectiveContract> contractList);
}
