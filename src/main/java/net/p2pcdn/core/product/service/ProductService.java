package net.p2pcdn.core.product.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qc.bean.result.Paging;
import net.p2pcdn.core.product.domain.Product;
import net.p2pcdn.core.product.domain.ProductType;
import net.p2pcdn.core.product.repository.ProductRepository;
import net.p2pcdn.core.product.vo.ProductVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProductService {
    @Resource
    private ProductRepository productRepository;

    public int saveProduct(ProductCreateCommand command) {
        Product product;
        boolean insert = false;
        if (command.getId() == null) {
            product = new Product();
            product.setCreateTime(new Date());
            product.setCreator(command.getCreator());
            insert = true;
        } else {
            product = productRepository.selectByPrimaryKey(command.getId());
        }
        product.setAmount(command.getAmount());
        product.setDescription(command.getDescription());
        product.setStatus(command.getStatus());
        product.setName(command.getName());
        product.setModifier(command.getCreator());
        product.setModifyTime(new Date());
        product.setOriginalPrice(command.getOriginalPrice());
        product.setPrice(command.getPrice());
        product.setPoster(command.getPoster());
        product.setRemark(command.getRemark());
        product.setSerial(command.getSerial());
        product.setSupportCoupon(command.isSupportCoupon());
        product.setType(command.getType());
        if (command.getRank() != null) {
            product.setRank(command.getRank());
        }
        if (ProductType.PACKAGE.getValue() == command.getType()) {
            if (command.getUnitType() == null) {
                throw new RuntimeException("套餐必须设置有效单位");
            }
            product.setUnitType(command.getUnitType());
            if (command.getStep() == null) {
                throw new RuntimeException("套餐必须设置单位有效期步长");
            }
            product.setStep(command.getStep());
        }else {
            if(StringUtils.isBlank(product.getName())){
                product.setName("BAG-"+ product.getPrice().toString());
            }
        }
        if (insert) {
            productRepository.insert(product);
        } else {
            productRepository.updateByPrimaryKey(product);
        }
        return product.getId();
    }

    public void publishProduct(int userId, int productId,int status) {
        Product product = productRepository.selectByPrimaryKey(productId);
        if (product != null) {
            product.setStatus(status);
            product.setModifyTime(new Date());
            product.setModifier(userId);
            this.productRepository.updateByPrimaryKey(product);
        }
    }

    public void setProductRank(int userId, int productId, float rank) {
        Product product = productRepository.selectByPrimaryKey(productId);
        if (product != null) {
            product.setRank(rank);
            product.setModifyTime(new Date());
            product.setModifier(userId);
            this.productRepository.updateByPrimaryKey(product);
        }
    }

    public Paging<ProductVO> query(String name, String serial, Integer status, Integer type, int page, int size) {
        PageHelper.startPage(page, size);
        Map<String, Object> params = new HashMap<>();
        params.put("name", name);
        params.put("serial", serial);
        params.put("status", status);
        params.put("type", type);
        List<ProductVO> products = this.productRepository.query(params);
        PageInfo<ProductVO> pageInfo = new PageInfo<>(products);
        return new Paging<>((int) pageInfo.getTotal(), products);
    }

    public Product getProduct(int productId) {
        return this.productRepository.selectByPrimaryKey(productId);
    }

}
