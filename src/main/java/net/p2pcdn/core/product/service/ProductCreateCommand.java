package net.p2pcdn.core.product.service;

import lombok.Getter;
import lombok.Setter;
import net.p2pcdn.core.product.domain.Product;

import java.math.BigDecimal;

@Setter
@Getter
public class ProductCreateCommand {
    private Integer id;
    private String serial;
    private String name;
    private String poster;
    private BigDecimal amount;
    private BigDecimal price;
    private BigDecimal originalPrice;
    private boolean supportCoupon;
    private int type;
    private Integer step;
    private Integer unitType;
    private String description;
    private String remark;
    private int status;
    private int creator;
    private Float rank;
}
