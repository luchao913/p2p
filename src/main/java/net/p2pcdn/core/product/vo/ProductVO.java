package net.p2pcdn.core.product.vo;

import lombok.Getter;
import lombok.Setter;
import net.p2pcdn.core.product.domain.Product;

@Getter
@Setter
public class ProductVO  extends Product {
    private int viewCount;
    private int orderCount;
    private String creatorName;
    private String modifierName;

    public boolean isVisible(){
        return this.getStatus() == Product.PUBLISHED;
    }
}
