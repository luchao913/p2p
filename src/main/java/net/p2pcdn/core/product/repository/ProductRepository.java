package net.p2pcdn.core.product.repository;

import net.p2pcdn.core.product.domain.Product;
import net.p2pcdn.core.product.vo.ProductVO;

import java.util.List;
import java.util.Map;

public interface ProductRepository {
    Product selectByPrimaryKey(Integer id);

    int insert(Product product);

    int updateByPrimaryKey(Product product);

    List<ProductVO> query(Map<String, Object> params);
}
