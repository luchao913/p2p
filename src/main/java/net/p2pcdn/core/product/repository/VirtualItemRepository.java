package net.p2pcdn.core.product.repository;

import net.p2pcdn.core.product.domain.VirtualItem;

public interface VirtualItemRepository {
    VirtualItem selectOne();
}

