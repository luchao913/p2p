package net.p2pcdn.core.product.domain;

/**
 * @author zx
 */

public enum ProductType {
    //套餐
    PACKAGE(1),
    //流量包
    BAG(2),
    //余额
    BALANCE(3);

    private final int value;

    ProductType(int value) {
        this.value = value;
    }

    public static ProductType get(int itemType) {
        for (ProductType productType : ProductType.values()) {
            if (productType.getValue() == itemType) {
                return productType;
            }
        }
        throw new RuntimeException("wrong itemType[" + itemType + "]");
    }

    public int getValue() {
        return this.value;
    }
}
