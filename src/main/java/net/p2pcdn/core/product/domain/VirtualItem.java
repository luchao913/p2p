package net.p2pcdn.core.product.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * 虚拟币=余额
 */
@Getter
@Setter
public class VirtualItem {
    private int id;
    private String name;
}
