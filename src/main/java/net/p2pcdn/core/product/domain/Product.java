package net.p2pcdn.core.product.domain;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * 产品
 */
@Getter
@Setter
public class Product {
    public final static int UN_PUBLISHED = 1;//未上架
    /**
     * 已上架
     */
    public final static int PUBLISHED = 2;

    public final static int MONTH = 1;
    public final static int YEAR = 2;

    private int id;
    private String serial;//产品ID
    private String name;//产品名称
    private String poster;//图片
    private BigDecimal amount;//流量
    private BigDecimal price;//现价
    //原价，划线价格
    private BigDecimal originalPrice;
    private boolean supportCoupon;
    //状态 : 下架，上架
    private int status;
    //产品类型：套餐，流量包
    private int type;
    //介绍
    private String description;
    //有效期单位天
    private Integer step;
    private Integer unitType;
    private int creator;
    private Date createTime;
    private int modifier;
    private Date modifyTime;
    private String remark;
    private float rank = 99999F;//排序

    public String getName() {
        if (StringUtils.isBlank(name)) {
            return amount + "/" + amount.toString();
        }
        return name;
    }

    public BigDecimal getOriginalPrice() {
        if (originalPrice == null) {
            originalPrice = price;
        }
        return originalPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Product)) {
            return false;
        }
        Product product = (Product) o;
        return getAmount().equals(product.getAmount()) &&
                isSupportCoupon() == product.isSupportCoupon() &&
                getType() == product.getType() &&
                getSerial().equals(product.getSerial()) &&
                getName().equals(product.getName()) &&
                getPrice().equals(product.getPrice()) &&
                getOriginalPrice().equals(product.getOriginalPrice());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSerial(), getName(), getAmount(), getPrice(), getOriginalPrice(), isSupportCoupon(), getType());
    }
}
